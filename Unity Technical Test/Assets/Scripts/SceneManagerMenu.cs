﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerMenu : MonoBehaviour
{
    public void LoadGame()
    {
        SceneManager.LoadScene("Gameplay01");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
