﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomItem : MonoBehaviour
{
    public Text roomName;
    LobbyManager manager;

    void Start()
    {
        manager = FindObjectOfType<LobbyManager>();
    }

    public void SetRoomName(string pRoomName)
    {
        roomName.text = pRoomName;
    }

    public void OnClickItem()
    {
        manager.JoinRoom(roomName.text);
    }

}
