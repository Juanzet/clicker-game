﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Camera cameraRay;
    public Text _playerCoinTxt;
    public Text win;

    private PhotonView _view;
    public int _playerCoin;

    public void Awake()
    {
        cameraRay = Camera.main;
        _view = GetComponent<PhotonView>();

    }

    void Update()
    {
        //if (_view.IsMine)
        //{
        //    GetComponentInChildren<Canvas>().gameObject.SetActive(true);
        //}

        if(_view.IsMine)
        {
            //if (GetComponent<GameManager>().pointsPlayer01 >= 100 || GetComponent<GameManager>().pointsPlayer02 >= 100)
            //{
            //    SceneManager.LoadScene("Win");
            //}

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = cameraRay.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.tag == "Shield")
                    {
                        hit.collider.GetComponent<Shield_Logic>()._lifePoints -= 1;
                        if (hit.collider.GetComponent<Shield_Logic>()._lifePoints == 0)
                        {
                            //_playerCoin += hit.collider.GetComponent<Shield_Logic>().coinCurrent += 10;
                            hit.collider.GetComponent<GameManager>().AddScore(10);
                            Destroy(hit.collider.gameObject);
                        }
                    }

                    if (hit.collider.tag == "Target")
                    {
                        hit.collider.GetComponent<Target_Logic>().TargetF();
                        Destroy(hit.collider.gameObject);

                    }

                    if (hit.collider.tag == "Coin")
                    {
                        //_playerCoin += hit.collider.GetComponent<CoinLogic>().coinCurrent += 5;
                        hit.collider.GetComponent<GameManager>().AddScore(5);
                        Destroy(hit.collider.gameObject);
                    }


                    if (hit.collider.tag == "blue_sphere")
                    {
                        //_playerCoin += hit.collider.GetComponent<SphereLogic>().coinCurrent += 2;
                        hit.collider.GetComponent<GameManager>().AddScore(2);
                        Destroy(hit.collider.gameObject);
                    }


                    if (hit.collider.tag == "YelloBlock")
                    {
                        //_playerCoin += hit.collider.GetComponent<Block_Logic>().coinCurrent += 1;
                        hit.collider.GetComponent<GameManager>().AddScore(1);
                        Destroy(hit.collider.gameObject);
                    }


                    if (hit.collider.tag == "RedBlock")
                    {
                        //_playerCoin -= hit.collider.GetComponent<Block_red_Logic>().coinCurrent = 5;
                        hit.collider.GetComponent<GameManager>().AddScore(-5);
                        Destroy(hit.collider.gameObject);
                    }


                    //Destroy(hit.collider.gameObject);
                    Debug.Log(hit.collider.tag);
                }
            }
            //_playerCoinTxt.text = "Puntos: " + _playerCoin.ToString();
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "blue_sphere" || other.gameObject.tag == "Coin" || other.gameObject.tag == "YelloBlock" || other.gameObject.tag == "Shield" || other.gameObject.tag == "Target")
        {
            _playerCoin--;
        }
    }

}
