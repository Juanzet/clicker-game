﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour
{
    public TextMesh textPoints;
    public PlayerController playerController;

    void Update()
    {
        textPoints.text = playerController._playerCoin.ToString(); 
    }
}
