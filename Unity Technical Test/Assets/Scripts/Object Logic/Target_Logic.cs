﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target_Logic : GameManager
{
    //TODO: Implementar logica, que al ser clickeado de bouns (hacer caer 3 monedas)
    int _points;

    [SerializeField] GameObject[] _prefab;

    public void TargetF()
    {
        
        StartCoroutine(TargetDeath());
    }

    private void Update()
    {

        coinCurrent = _points;
    }

    IEnumerator TargetDeath()
    {
        //Vector3 mPosition = new Vector3(Random.Range(0,4),Random.Range(1,5),Random.Range(2,6));
        //Instantiate(prefab, mPosition, Quaternion.identity);
        while (true)
        {
            var wanted = Random.Range(1f, 2f); // moneda 1
            var wanted2 = Random.Range(5f, 6f); // moneda 2
            var wanted3 = Random.Range(8f, 9f); // moneda3
            var position = new Vector3(wanted, transform.position.y); // moneda 1
            var position2 = new Vector3(wanted2, transform.position.y); // moneda 2
            var position3 = new Vector3(wanted3, transform.position.y); // moneda 3
            GameObject go = Instantiate(_prefab[Random.Range(0, _prefab.Length)], position, Quaternion.identity); // moneda 1
            GameObject go2 = Instantiate(_prefab[Random.Range(0, _prefab.Length)], position2, Quaternion.identity); // moneda 2
            GameObject go3 = Instantiate(_prefab[Random.Range(0, _prefab.Length)], position3, Quaternion.identity); // moneda 3
            yield return new WaitForSeconds(5f);
            Destroy(go, 3f);
            Destroy(go2, 3f);
            Destroy(go3, 3f);

            yield return new WaitForSeconds(5f);
        }

        
    }
}
