﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawn : MonoBehaviour
{
    [SerializeField] GameObject[] _objectPrefab;
    [SerializeField] float _spawnSec;
    [SerializeField] float _minTrans;
    [SerializeField] float _maxTrans;

    void Start()
    {
        StartCoroutine(ObjectSpawnIE());
    }

    IEnumerator ObjectSpawnIE()
    {
        while(true)
        {
            var wanted = Random.Range(_minTrans, _maxTrans);
            var position = new Vector3(wanted, transform.position.y);
            GameObject go = Instantiate(_objectPrefab[Random.Range(0, _objectPrefab.Length)], position, Quaternion.identity);
            yield return new WaitForSeconds(_spawnSec);
            Destroy(go, 5f);
        }
        
    }
}
