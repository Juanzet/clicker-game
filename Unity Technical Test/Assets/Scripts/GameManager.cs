﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine.UI;
using Photon.Chat;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourPunCallbacks
{
    public Text player01;
    public Text player02;
    //public Text currentCointsText;
    public int coinCurrent;

    public int pointsPlayer01;
    public int pointsPlayer02;

    void Start()
    {
        //InvokeRepeating("ExecuteBetween",1f,0.5f);   
    }
    void Update()
    {
        if(pointsPlayer01 >=100 || pointsPlayer02 >= 100)
        {
            SceneManager.LoadScene("Win");
        }

        if (PhotonNetwork.PlayerList.Count() < 1)
        {
            SceneManager.LoadScene("MainMenu");
        }

        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player.IsLocal)
            {
                //contPoints = GetComponent<PlayerController>()._playerCoin;
                //player.AddScore(contPoints);
            }
            
        }
        //player01.text = GetComponent<PlayerController>()._playerCoin.ToString();
        //player02.text = GetComponent<PlayerController>()._playerCoin.ToString();
    }

    public void AddScore(int _points)
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player.IsLocal)
            {
                player.AddScore(_points);
            }

        }

        //photonView.RPC("CheckPoints", RpcTarget.All);
    }

    [PunRPC]
    public void CheckPoints()
    {
        if (PhotonNetwork.PlayerList[0].IsLocal)
        {
            pointsPlayer01 = PhotonNetwork.PlayerList[0].GetScore();   
        }

        if (PhotonNetwork.PlayerList[1].IsLocal)
        {
            pointsPlayer02 = PhotonNetwork.PlayerList[1].GetScore();
        }


        player01.text = pointsPlayer01.ToString();
        player02.text = pointsPlayer02.ToString();

    }

    //public void ExecuteBetween()
    //{
    //    photonView.RPC("CheckPoints", RpcTarget.All);
    //}

}    
