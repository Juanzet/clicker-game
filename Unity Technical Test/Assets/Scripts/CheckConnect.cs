﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CheckConnect : MonoBehaviourPunCallbacks
{

    public InputField usernameInput;
    public Text buttonText;
    public GameObject textLoading;

    void Start()
    {
  
        textLoading.SetActive(false);
    }

    public void OnClickConnect()
    {
        if(usernameInput.text.Length >= 1)
        {
            PhotonNetwork.NickName = usernameInput.text;
            buttonText.text = "Connecting . . .";
            textLoading.SetActive(true);
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.ConnectUsingSettings();
           
        }
    }

    public override void OnConnectedToMaster()
    {
        //PhotonNetwork.JoinLobby();
        SceneManager.LoadScene("MainMenu");
    }

    //public override void OnJoinedLobby()
    //{
    //    SceneManager.LoadScene("MainMenu");
    //}
}
