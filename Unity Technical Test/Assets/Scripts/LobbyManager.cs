﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    //variables comunes
    public InputField roomInputField;
    #region GameObjects
    public GameObject lobbyPanel;
    public GameObject roomPanel;
    public GameObject createRoomBotton;
    public GameObject joinedARandomRoom;
    public GameObject leaveButton;
    public GameObject playButton;
    #endregion
    public Text roomName;
    public Transform contentObject;

    // float para updatear cada X tiempo
    public float timeBetweenUpdates = 1.5f;
    float nextUpdateTime;

    // variables de script
    public RoomItem roomItemPrefab;
    List<RoomItem> roomItemsList = new List<RoomItem>();

    // variable lista player
    public List<PlayerItem> playerItemList = new List<PlayerItem>();
    public PlayerItem playerItemPrefab;
    public Transform playerItemParent;

    void Start()
    {
        PhotonNetwork.JoinLobby();
    }

    public void JoinInRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public void OnClickCreate()
    {
        if(roomInputField.text.Length >= 1)
        {
            PhotonNetwork.CreateRoom(roomInputField.text, new RoomOptions() {MaxPlayers = 2 });
        }
    }

    public override void OnJoinedRoom()
    {
        leaveButton.SetActive(true);
        joinedARandomRoom.SetActive(false);
        createRoomBotton.SetActive(false);
        lobbyPanel.SetActive(false);
        roomPanel.SetActive(true);
        roomName.text = "Room: " + PhotonNetwork.CurrentRoom.Name;
        UpdatePlayerList();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        if(Time.time >= nextUpdateTime)
        {
            UpdateRoomList(roomList);
            nextUpdateTime = Time.time + timeBetweenUpdates;
        }
        
    }

    void UpdateRoomList(List<RoomInfo> list) // metodo que actualiza la lista elminando los viejos y instanciando los nuevos items con el metodo creado en el script RoomItem
    {
        foreach(RoomItem item in roomItemsList)
        {
            Destroy(item.gameObject);
        }
        roomItemsList.Clear();

        foreach (RoomInfo room in list)
        {
            RoomItem newRoom = Instantiate(roomItemPrefab, contentObject);
            newRoom.SetRoomName(room.Name);
            roomItemsList.Add(newRoom);
        }
    }

    public void JoinRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public void OnClickLeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        leaveButton.SetActive(false);
        roomPanel.SetActive(false);
        lobbyPanel.SetActive(true);
        joinedARandomRoom.SetActive(true);
        createRoomBotton.SetActive(true);
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    void UpdatePlayerList()
    {
        foreach (PlayerItem item in playerItemList)
        {
            Destroy(item.gameObject);
        }
        playerItemList.Clear();

        if(PhotonNetwork.CurrentRoom == null)
        {
            return;
        }

        foreach  (KeyValuePair<int, Player> player in PhotonNetwork.CurrentRoom.Players)
        {
            PlayerItem newPlayerItem = Instantiate(playerItemPrefab, playerItemParent);
            newPlayerItem.SetPlayerInfo(player.Value);
            playerItemList.Add(newPlayerItem);
        }
       
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        UpdatePlayerList();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        UpdatePlayerList();
    }
    void Update()
    {
        if (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount >=2) // cambiar a 2
        {
            playButton.SetActive(true);
        } else
        {
            playButton.SetActive(false);
        }
    }

    public void OnClickPlayButton()
    {
        PhotonNetwork.LoadLevel("Gameplay01");
    }
}
